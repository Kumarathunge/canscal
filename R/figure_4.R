#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

#WTC3 sun-shade comparison-default gammastar

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

path<-getwd()
fs <- makecurves(path,"/data/shadeleafaci.csv",figname="WTC3")
ds <- makedata(path,"/Data/shadeleafaci.csv",fs)
ds$layer<-factor("shade")

fsh <- makecurves(path,"/data/sunleafaci.csv",figname="WTC3")
dsh <- makedata(path,"/Data/sunleafaci.csv",fsh)
dsh$layer<-factor("sun")

# with(dsh,plot(Vcmax,Jmax,col=temp_treatment,pch=16,ylim=c(50,200),xlim=c(50,200)))
# with(ds,points(Vcmax,Jmax,col=temp_treatment,ylim=c(50,200),xlim=c(50,200)))

sun_shade_data<-rbind.fill(ds,dsh)


# test Vcmax
lmshade<-lm(Vcmax~layer,data=sun_shade_data)
summary(lmshade)
car::Anova(lmshade)


# test Jmax
lmj<-lm(Jmax~layer,data=sun_shade_data)
summary(lmj)
car::Anova(lmj)

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

sun_params<-summaryBy(Vcmax+Jmax~leaf,data=dsh,FUN=c(mean,std.error),na.rm=T)
sun_params$Leaf<-"sun"
shade_params<-summaryBy(Vcmax+Jmax~leaf,data=ds,FUN=c(mean,std.error))
shade_params$Leaf<-"shade"

sun_params<-rbind(sun_params,shade_params)

sun_params$CI_L_vcmax<-with(sun_params,Vcmax.mean-Vcmax.std.error)
sun_params$CI_U_vcmax<-with(sun_params,Vcmax.mean+Vcmax.std.error)

sun_params$CI_L_jmax<-with(sun_params,Jmax.mean-Jmax.std.error)
sun_params$CI_U_jmax<-with(sun_params,Jmax.mean+Jmax.std.error)

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

tiff(file="output/manuscript_figures/figure_4.tiff",width=6,height=8,units="in",res=300)
par(mar=c(2,5,0.5,.5),mfrow=c(1,2),cex.lab=1.5,cex.axis=1.5,oma=c(0,0,0,0))


#-- Vcmax
barplot2(height=sun_params$Vcmax.mean,plot.ci=T,ci.l=sun_params$CI_L_vcmax,ci.u=sun_params$CI_U_vcmax,
         yaxt="n",ci.width=0.2,col=c("red","darkgrey"),
         names.arg=c("Sun","Shade"),ylim=c(0,200))



magaxis(side=c(2,4),labels=c(1,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.2,majorn=3)
# title(main=expression(V[cmax25]),cex.main=1.5)
# title(xlab="Canopy position",outer=F,line=3)
title(ylab=expression(V[cmax25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=2.2)


text(110,"**",cex=2,adj=-.5)
legend("topright",expression((bold(a))),bty="n",cex=1.3)

#Jmax
barplot2(height=sun_params$Jmax.mean,plot.ci=T,ci.l=sun_params$CI_L_jmax,ci.u=sun_params$CI_U_jmax,
         yaxt="n",ci.width=0.2,col=c("red","darkgrey"),
         names.arg=c("Sun","Shade"),ylim=c(0,200))



magaxis(side=c(2,4),labels=c(1,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.2,majorn=3)
# title(main=expression(J[max25]),cex.main=1.5)
title(ylab=expression(J[max25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=2.2)

text(170,"**",cex=2,adj=-.5)
legend("topright",expression((bold(b))),bty="n",cex=1.3)



dev.off()

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------



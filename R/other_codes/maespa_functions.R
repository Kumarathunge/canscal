startover <- function(){
  file.copy(dir("original_inputfiles", full.names=TRUE),"maespa",overwrite=TRUE)
}
cleanup <- function()unlink(Sys.glob("*.dat"))



make_met <- function(ch){
  met <- subset(wtcflux, chamber == ch, select=c(DateTime, Tair_al, VPDair, PAR))
  met <- met[order(met$DateTime),-1]  # order by DateTime; remove DateTime
  
  # Fill NA with last non-NA
  met <- within(met, {
    Tair_al <- na.locf(Tair_al)
    VPDair <- na.locf(VPDair)
    PAR <- na.locf(PAR)
  })
  
  # CO2 must be added; data in wtcflux too noisy
  met$CA <- 400
  
  # Units VPD are Pa
  met$VPDair <- 1000 * met$VPDair
  
  replacemetdata(met, "maespa/met.dat", columns=c("TAIR","VPD","PAR","CA"), 
                 "maespa/met.dat", setdates=FALSE, khrs=24)
  replacePAR("maespa/met.dat", "khrsperday", "metformat", 24)
  replacePAR("maespa/met.dat", "startdate", "metformat", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/met.dat", "enddate", "metformat", format(endDate, "%d/%m/%y"))
}


make_trees <- function(ch){
  
  hd <- subset(treeh, chamber == ch)
  replacePAR("maespa/trees.dat","values","allhtcrown",round(hd$Plant_height / 100,2))
  replacePAR("maespa/trees.dat","nodates","allhtcrown",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allhtcrown",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allradx",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allradx",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allradx",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allrady",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allrady",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allrady",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "allhttrunk", 0.01)
  
  replacePAR("maespa/trees.dat", "values", "alldiam", round(hd$diameter/1000,3))
  replacePAR("maespa/trees.dat","nodates","alldiam",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alldiam",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "alllarea", round(hd$leafArea,2))
  replacePAR("maespa/trees.dat","nodates","alllarea",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alllarea",format(hd$Date,"%d/%m/%y"))
  
  #replacePAR("maespa/trees.dat", "xycoords", "xy", make_stand())
}


make_con <- function(){
  
  replacePAR("maespa/confile.dat", "startdate", "dates", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat", "enddate", "dates", format(endDate, "%d/%m/%y"))
  
  # Use Medlyn 2011 model of gs.
  replacePAR("maespa/confile.dat", "modelgs", "model", 4)
  replacePAR("maespa/confile.dat", "modelrw ", "model", 0)
}


make_phy <- function(){
  
  replacePAR("maespa/phy.dat","g1","bbmgs",2.2) #g1 fot top canopy leaves (no difference between control and warmed treatments)
  
  # only Vcmax25, Jmax25 and delsc showed difference between seasons
  
  #replacePAR("maespa/phy.dat","values","jmax",159)
  replacePAR("maespa/phy.dat","values","jmax",c(206,155.9,155.9))
  replacePAR("maespa/phy.dat","nodates","jmaxcon",3)
  replacePAR("maespa/phy.dat","dates","jmax",c("14/09/13","30/11/13","01/03/14"))
  
  #replacePAR("maespa/phy.dat","values","vcmax",80)
  replacePAR("maespa/phy.dat","values","vcmax",c(115.3,90.7,90.7))
  replacePAR("maespa/phy.dat","nodates","vcmaxcon",3)
  replacePAR("maespa/phy.dat","dates","vcmax",c("14/09/2013","30/12/13","01/03/14"))
  
  # to do : T-response, Rd, g0
  
  replacePAR("maespa/phy.dat","theta","jmaxpars",0.4)
  replacePAR("maespa/phy.dat","eavj","jmaxpars",21)
 
  replacePAR("maespa/phy.dat","delsj","jmaxpars",629)
  replacePAR("maespa/phy.dat","ajq","jmaxpars",0.28)
  
  replacePAR("maespa/phy.dat","eavc","vcmaxpars",58000)
  replacePAR("maespa/phy.dat","delsc","vcmaxpars",635)
  #replacePAR("maespa/phy.dat","delsc","vcmaxpars",c(635,629,636))
  #replacePAR("maespa/phy.dat","dates","delsc",c("14/09/2013","30/12/13","01/03/14"))
}


make_str <- function(){
  
  replacePAR("maespa/str.dat", "elp", "lia", 1.0)  # spherical LIA
  replacePAR("maespa/str.dat", "cshape", "canopy", "CYL")  # canopy shape
  
}


run_chamber <- function(ch){
  o <- getwd()
  on.exit(setwd(o))
  
  startover()
  
  make_str()
  make_con()
  make_phy()
  
  make_trees(ch)
  make_met(ch)
  
  setwd("maespa")
  shell("MAESPA64.exe", intern=TRUE)
  # h <- readlayflux() # get flux layers
  h <- readhrflux() # get total flux
  message(sprintf("Chamber %s simulation complete.",ch))
  
  h$chamber <- ch
  h$DateTime <- seq(from=as.POSIXct(as.character(startDate), tz="UTC"), length=nrow(h), by="1 hour")
  return(h)
}


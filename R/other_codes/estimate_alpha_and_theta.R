# get theta and alpha from LRC######

lrc.df <- read.csv("data/FACE_P0069_RA_GASEX-LRC_20141009-20170209_L1-V2.csv")

lrc.df <-lrc.df[lrc.df$Datatype ==  "compLRC",]

get_gam<-function(data){
  TsK<-data$Tleaf+273.15 
  gamastar<-42.75 * exp((37.8*(TsK - 298.15))/(298.15*0.008314*TsK))
  return(gamastar)
}

# arrhenius(42.75,37830.0/1000, TTK =c(lrc.df$Tleaf + 273.15))

lrc.df$gammas <- get_gam(lrc.df)

lrc.df$apar <- lrc.df$PARi * (1- 0.093 -0.082)

lrc.df.low.par <- lrc.df[lrc.df$PARi < 100,]

fit.a.lrc <- lm(Photo~apar,
                
                data = lrc.df.low.par)

alpha.a <- coef(summary(fit.a.lrc))[2]



alpha.j <- mean(4*alpha.a*(lrc.df.low.par$Ci + 2*lrc.df.low.par$gammas)/
                  
                  (lrc.df.low.par$Ci - lrc.df.low.par$gammas))



restult.lrc <- coef(nls(Photo~
                          
                          alpha.a * apar+ Pm - ((alpha.a * apar+ Pm)^2-4*alpha.a * apar*Pm*theta)^0.5/2/theta,
                        
                        data=lrc.df,start = list(Pm = 30, theta = 0.5)))



restult.lrc$alpha.j <- alpha.j



restult.lrc <- as.data.frame(restult.lrc)

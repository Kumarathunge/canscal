
# fit ACi curves measured at 25C (Mike Aspinwall data)

aci_dat<-read.csv("data/WTC_TEMP_CM_GX-ACI25_20130705-20140422_L1_v1.csv")

# #add curev number

aci_dat$Curve <- c(1)
Curve <- c()
count <- 1  
for (i in 2:length(aci_dat$Code)){
  
  ifelse(aci_dat$Code[i-1] == aci_dat$Code[i],count <- count,count <- count + 1)
  Curve[i] <- count 
}

aci_dat$Curve[2:length(aci_dat$Code)] <- na.omit(Curve)

# to remove unnecessary data points (This remove repeated measurements of Asat at ambient CO2 at the beggining of each ACi curve)

aci_dat<-subset(aci_dat,aci_dat$Tofit==1)

write.csv(aci_dat,"data/WTC_TEMP_CM_GX-ACI25_20130705-20140422_L1_v2.csv")


# fit ACi curves

path<-getwd()

f3_25 <- makecurves(path,"/data/WTC_TEMP_CM_GX-ACI25_20130705-20140422_L1_v2.csv",figname="WTC3_ACi_25C")
d3_25 <- makedata(path,"/Data/WTC_TEMP_CM_GX-ACI25_20130705-20140422_L1_v2.csv",f3_25)

# get mean values of Vcmax and Jmax (at 25C)

aci_mean<-summaryBy(Vcmax+Jmax~Date+T_treatment,data=d3_25,FUN=c(mean,std.error))


# add preceding 30 days air temperature


aci_mean$Tavg_30<-NA
aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2013-07-05")]<-wtcMet(path=paste0(path,"/data"),
                                                                                         fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-07-05")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2013-07-05")]<-wtcMet(path=paste0(path,"/data"),
                                                                                          fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-07-05")[[4]][[2]]


aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2013-08-21")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-08-21")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2013-08-21")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-08-21")[[4]][[2]]


aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2013-09-19")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-09-19")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2013-09-19")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-09-19")[[4]][[2]]


aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2013-10-18")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-10-18")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2013-10-18")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-10-18")[[4]][[2]]


aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2013-12-03")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-12-03")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2013-12-03")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-12-03")[[4]][[2]]

aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2014-01-13")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-01-13")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2014-01-13")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-01-13")[[4]][[2]]

aci_mean$Tavg_30[which(aci_mean$T_treatment=="ambient" & aci_mean$Date=="2014-04-22")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-04-22")[[4]][[1]]
aci_mean$Tavg_30[which(aci_mean$T_treatment=="elevated" & aci_mean$Date=="2014-04-22")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-04-22")[[4]][[2]]

aci_mean$Date<-as.Date(aci_mean$Date)

#----------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------------------

pdf("output/manuscript_figures/vcmax_jmax_25.pdf",height=8,width=4)
par(mar=c(2,4,.5,.5),oma=c(3,0,0,0),mfrow=c(2,1))

with(aci_mean,plot(Tavg_30,Vcmax.mean,ylim=c(50,250),xlim=c(10,30),xlab="",ylab="",bg=alpha("red",0.9),pch=21,cex=.2))
adderrorbars(aci_mean$Tavg_30,aci_mean$Vcmax.mean,aci_mean$Vcmax.std.error,"updown")
with(aci_mean,points(Tavg_30,Vcmax.mean,ylim=c(50,250),xlim=c(10,30),xlab="",ylab="",bg=alpha("red",0.9),pch=21,cex=1.5))

# title(xlab=expression(T[growth]~(degree*C)),outer=F,cex.lab=1.5)
title(ylab=expression(V[cmax25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=2)

lmfits2<-summary(lm(Vcmax.mean~Tavg_30,data=subset(aci_mean,aci_mean$Vcmax.mean<150),weights=1/Vcmax.std.error))
ablineclip(lmfits2$coefficients[1],lmfits2$coefficients[2],x1=8,x2=30,y1=230,y2=80,lwd=2)

mylabel_1 = bquote(italic(r)^2 == .(format(lmfits2$adj.r.squared, digits = 1)))
mylabel_2 = bquote(italic(y) == .(format(lmfits2$coefficients[2], digits = 2))~italic(x) - .(format(abs(lmfits2$coefficients[1]), digits = 2)))

# text(x = -1.0, y = 19, labels = mylabel_2)
# text(x = -2.5, y = 18, labels = mylabel_1)

text(x = 20, y = 245, labels = mylabel_2,cex=1.3)
text(x = 20, y = 230, labels = mylabel_1,cex=1.3)

lma<-lmer(Vcmax.mean~Tavg_30+(1|T_treatment),data=subset(aci_mean,aci_mean$T_treatment=="ambient"),weights=1/Vcmax.std.error)
summary(lma)
rsquared.glmm(lmm_275)
Anova(lma,test="F")

with(aci_mean,plot(Tavg_30,Jmax.mean,ylim=c(50,250),xlim=c(10,30),xlab="",ylab="",bg=alpha("red",0.9),pch=21,cex=.2))
adderrorbars(aci_mean$Tavg_30,aci_mean$Jmax.mean,aci_mean$Jmax.std.error,"updown")
with(aci_mean,points(Tavg_30,Jmax.mean,ylim=c(50,250),xlim=c(10,30),xlab="",ylab="",bg=alpha("red",0.9),pch=21,cex=1.5))


# legend("topleft",legend=c("Ambient",expression(3~degree*C~Warmed)),bty="n",pt.bg=c("black","red"),pch=21,cex=1,ncol=2)

title(xlab=expression(T[growth]~(degree*C)),outer=T,cex.lab=1.5,line=1,adj=0.625)
title(ylab=expression(J[max25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=2)


lmfits<-summary(lm(Jmax.mean~Tavg_30,data=aci_mean,weights=1/Jmax.std.error))

ablineclip(lmfits$coefficients[1],lmfits$coefficients[2],x1=8,x2=30,y1=230,y2=80,lwd=2)

# legend("topleft",legend=c(paste("Y =",round(lmfits$coefficients[2],2),"X +",round(lmfits$coefficients[1],2)),
#                               paste("R2 =",round(lmfits$adj.r.squared,2))),bty="n",ncol=1,cex=.7)


mylabel_1 = bquote(italic(r)^2 == .(format(lmfits$adj.r.squared, digits = 1)))
mylabel_2 = bquote(italic(y) == .(format(lmfits$coefficients[2], digits = 1))~italic(x) - .(format(abs(lmfits$coefficients[1]), digits = 2)))

# text(x = -1.0, y = 19, labels = mylabel_2)
# text(x = -2.5, y = 18, labels = mylabel_1)

text(x = 20, y = 75, labels = mylabel_2,cex=1.3)
text(x = 20, y = 60, labels = mylabel_1,cex=1.3)


dev.off()

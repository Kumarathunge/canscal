#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


# run MAESPA with two season parameter setting
# all temperature response parameters specified for one canopy layer
# Vcmax and Jmax specified for two dates (seasons) and six canopy layers
# radiation interception - 6 layers

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
source("R/maespa_functions_control.R")
source("R/install_maespa.R")
source("R/loadLibraries.R")
source("R/data_processing.R")


# one season photosynthetic parameters assigned for one canopy layer

df<-NULL
dat.plot.a<-NULL


source("R/new_functions_for_wtc3.R")
maes_4 <-do.call(rbind,lapply(insidechambers_con, function(x)run_chamber(x,jmaxnodates=2,jmaxnolayers=6,
                                                                         jmax=c(206,180.5,168.9,158.2,148.1,116.7,(c(206,180.5,168.9,158.2,148.1,116.7)*0.76)),
                                                                         jmaxdates=c("14/09/13","01/12/13"),
                                                                         vcmaxnodates=2,vcmaxnolayers=2,
                                                                         vcmax=c(115.3,104.7,99.8,95.1,90.7,85.9,c(115.3,104.7,99.8,95.1,90.7,85.9)*.8),
                                                                         vcmaxdates=c("14/09/2013","30/11/2013"),
                                                                         theta=0.475,eavj=25500,delsj=635,ajq=0.26,eavc=56800,delsc=633,
                                                                         rday=c(1.4),dayresp=.7,q10ndates=1,rdnodates=1,rddates=c("14/09/13"),
                                                                         q10=c(0.07),q10dates=c("14/09/13"))))

# merge measured flux with modelled flux
# convert units 
# substract wood respiration

df<-merge_flux(maes_4)

# get subset of high PAR (PAR>1200 for temperature response)

dat.plot.a<-subset(df,df$PAR.x>1200 & df$Date<as.Date("2014-01-14"))
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# plot measured vs modelled fluc and temperature response at high PAR 

pdf("output/manuscript_figures/figure_4.pdf",height=6,width=12)
par(mar=c(5.5,5.5,2,1),oma=c(0,0,0,0),mfrow=c(1,2))
plot_flux(Title=NULL)
legend("topleft",expression((bold(a))),bty="n",cex=1.5)
plot_tresponse_wtc()
legend("topleft",expression((bold(b))),bty="n",cex=1.5)
dev.off()

# rm(list = ls())
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


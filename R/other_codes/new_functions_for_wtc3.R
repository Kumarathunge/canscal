

#run MAESPA for ambient T chambers
# 
# library(Maeswrap)
# library(HIEv)
# library(gplots)
# library(zoo)
# library(doBy)
# library(plover)
# library(scales)
#source("R/maespa_functions_control.R")

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------



# source("R/data_processing.R")
# source("R/install_maespa.R")

setToken("xLh3r7p426yVepvT7qVs")

if(!dir.exists("cache"))dir.create("cache")
setToPath("cache")

if(!dir.exists("maespa"))dir.create("maespa")


# Download MAESPA if not available (or use this to update)
if(!file.exists("maespa/MAESPA64.exe")){
  install_maespa("maespa")
}

# with(wtcflux,plot(DateTime,PAR))
# with(wtcflux,points(DateTime,PAR.WTC,col="red"))
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# Read WTC flux data
wtcflux <- make_or_read("cache/wtcflux.rds", make_wtcflux)
wtcflux$chamber<-factor(wtcflux$chamber)
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


# correction for PAR and LA (2018/09/27)

# wtc_met_data<-read.csv("data/met_la_data_all_processed_KM.csv")
# wtc_met_data$DateTime<-as.POSIXct(wtc_met_data$DateTime,format="%d/%m/%Y %H:%M",tz="UTC")
# 
# # names(wtc_met_data)[c(9)]<-"la_new"
# 
# wtcflux<-merge(wtcflux[c(1:4,10,15)],wtc_met_data[c(2,3,7,9)],by=c("chamber","DateTime"))
# names(wtcflux)[c(7,8)]<-c("PAR","leafArea")
# # have to change variable names
# 
# # wtcflux <- make_wtcflux()
# # write.csv(wtcflux,"data/wtcflux_processed.csv")

wtcflux<-read.csv("data/wtcflux_processed.csv")
wtcflux$Date<-as.Date(wtcflux$DateTime,"%d/%m/%Y")
wtcflux$DateTime <- as.POSIXct(wtcflux$DateTime,format="%d/%m/%Y %H:%M",tz="UTC")

# start and end dates of simulation (used throughout as flux measurements available)
startDate <- min(wtcflux$Date)
endDate <- max(wtcflux$Date)

# Read plant height and crown diameter data (WTC3)
treeh <- make_or_read("cache/treeh.rds", make_treeh)


# Chamber codes 
insidechambers_con <- sprintf("C%02.f",(c(1,3,5,7,9,11)))

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------




#run MAESPA for Control treatment 

# get the required functions
# this functions make "confile", "met", "str" and "trees" files for each chamber

# source("R/maespa_functions_control.R") 

#get functions to set "phy" file for control T_treatment


#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------



make_phy <- function(jmaxnodates,jmaxnolayers,jmax,jmaxdates,vcmaxnodates,vcmaxnolayers,vcmax,vcmaxdates,
                     theta,eavj,delsj,ajq,eavc,delsc,rdnodates,rday,rddates,dayresp=1,q10ndates,q10,q10dates){
  
  replacePAR("maespa/phy.dat","g1","bbmgs",2.2) 
  replacePAR("maespa/phy.dat","dates","bbmgs","14/09/13")
  
  
  # Jmax parameters
  
  replacePAR("maespa/phy.dat","nodates","jmaxcon",jmaxnodates)
  replacePAR("maespa/phy.dat","nolayers","jmaxcon",jmaxnolayers)
  replacePAR("maespa/phy.dat","values","jmax",jmax) 
  replacePAR("maespa/phy.dat","dates","jmax",jmaxdates)                         
  
  
  # Vcmax parameters
  
  replacePAR("maespa/phy.dat","nodates","vcmaxcon",vcmaxnodates)
  replacePAR("maespa/phy.dat","nolayers","vcmaxcon",vcmaxnolayers) 
  replacePAR("maespa/phy.dat","values","vcmax",vcmax)
  replacePAR("maespa/phy.dat","dates","vcmax",vcmaxdates)
  
  
  # Temperature response of Vcmax and Jmax
  
  replacePAR("maespa/phy.dat","theta","jmaxpars",0.45) #all parameters are for summer 2013
  replacePAR("maespa/phy.dat","eavj","jmaxpars",eavj)
  
  replacePAR("maespa/phy.dat","delsj","jmaxpars",delsj)
  replacePAR("maespa/phy.dat","ajq","jmaxpars",ajq)
  
  replacePAR("maespa/phy.dat","eavc","vcmaxpars",eavc)
  replacePAR("maespa/phy.dat","delsc","vcmaxpars",delsc)
  
  #Rday
  replacePAR("maespa/phy.dat","nodates","rdcon",rdnodates) 
  replacePAR("maespa/phy.dat","values","rd",rday) 
  replacePAR("maespa/phy.dat","dates","rd",rddates) 
  replacePAR("maespa/phy.dat","dayresp","rdpars",dayresp)
  
# Tresponse of Rday
  replacePAR("maespa/phy.dat","rates","folq10",q10)
  replacePAR("maespa/phy.dat","ndates","folq10",q10ndates)
  replacePAR("maespa/phy.dat","dates","folq10",q10dates)
  
  }


# change temperature response parameters using make_phy function for each run
# so the run_chamber function should update for each simulation 

run_chamber <- function(ch,parlayers=6,nopints=12,jmaxnodates,jmaxnolayers,jmax,jmaxdates,vcmaxnodates,vcmaxnolayers,vcmax,vcmaxdates,
                        theta,eavj,delsj,ajq,eavc,delsc,rdnodates,rday,rddates,dayresp=dayresp,q10ndates,q10,q10dates){
  o <- getwd()
  on.exit(setwd(o))
  
  startover()
  
  make_str()
  make_con(parlayers,nopints)
  
  
  make_phy(jmaxnodates,jmaxnolayers,jmax,jmaxdates,vcmaxnodates,vcmaxnolayers,vcmax,vcmaxdates,
           theta,eavj,delsj,ajq,eavc,delsc,rdnodates,rday,rddates,dayresp=1,q10ndates,q10,q10dates)
  
  make_trees(ch)
  make_met(ch)
  
  setwd("maespa")
  shell("MAESPA64.exe", intern=TRUE)
  # h <- readlayflux()
  h <- readhrflux()
  message(sprintf("Chamber %s simulation complete.",ch))
  
  h$chamber <- ch
  h$DateTime <- seq(from=as.POSIXct(as.character(startDate), tz="UTC"), length=nrow(h), by="1 hour")
  return(h)
}


#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# get layer output

run_chamber_layers <- function(ch,jmaxnodates,jmaxnolayers,jmax,jmaxdates,vcmaxnodates,vcmaxnolayers,vcmax,vcmaxdates,
                        theta,eavj,delsj,ajq,eavc,delsc,rdnodates,rday,rddates,dayresp=dayresp,q10ndates,q10,q10dates){
  o <- getwd()
  on.exit(setwd(o))
  
  startover()
  
  make_str()
  make_con()
  
  
  make_phy(jmaxnodates,jmaxnolayers,jmax,jmaxdates,vcmaxnodates,vcmaxnolayers,vcmax,vcmaxdates,
           theta,eavj,delsj,ajq,eavc,delsc,rdnodates,rday,rddates,dayresp=1,q10ndates,q10,q10dates)
  
  make_trees(ch)
  make_met(ch)
  
  setwd("maespa")
  shell("MAESPA64.exe", intern=TRUE)
  h <- readlayflux()
  h<-h[with(h, order(Date, Hour)),]
  
  
  # h <- readhrflux()
  message(sprintf("Chamber %s simulation complete.",ch))
  
  h$chamber <- ch
  h$DateTime <- seq(from=as.POSIXct(as.character(startDate), tz="UTC"), length=nrow(h), by="1 hour")
  return(h)
}


#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


# Function to merge with measured chamber flux

merge_flux<-function(modelledflux){
  
  flux_control<-subset(wtcflux,wtcflux$chamber %in% c(insidechambers_con))
  df <- merge(flux_control, modelledflux, by=c("chamber","DateTime"), all=TRUE)
  
   
  #get flux per leaf area
  
  df$hrPs<-df$hrPs-df$hrRmW #(substract wood respiration before convert flux per leaf area basis) 
  # df$hrPs<-df$hrPs-df$hrRf #(substract wood respiration assuming wood respiration=leaf respiration) 
  df$hrPs_la<-with(df,hrPs/leafArea) #units for MAESPA hourly flux data is mumol/tree/s. convert it to mumol/m2 (LA)/s
  
  df$hrPs_la_m<-with(df,FluxCO2*1000/leafArea) #units for wtc3 hourly flux data is mmol/s. convert it to mumol/m2 (LA)/s
  
  df$residuals<-with(df,hrPs_la_m-hrPs_la)
  
  return(df)
}

# df<-merge_flux(maes_con)
# # get flux at high PAR to plot the temperature response
# 
# dat.plot.a<-subset(df,df$PAR.x>1200)

#--------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------

# plot measured vs modelled flux

plot_flux<-function(Title){
  
  
  # windows(40,40);par(mfrow=c(1,1),mar=c(0,0,0,0),oma=c(5,7,2,2));par(las=1)
  with(df,plot(hrPs_la_m~hrPs_la,xlim=c(-5,20),ylim=c(-5,20),col=alpha("lightgray",0.1),xlab="",ylab="",axes=F))
  abline(0,1)
  
  magaxis(side=c(1:4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.5,majorn=4)
  
  lmod<-summary(lm(hrPs_la_m~hrPs_la,data=df))
  
  abline(lmod$coefficients[1],lmod$coefficients[2]
         ,col="black",lwd=3,lty=2)
  
  title(xlab=expression(A[modelled]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=2)
  title(ylab=expression(A[measured]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=2,line=1.7)
  
   
  mylabel_1 = bquote(italic(r)^2 == .(format(lmod$adj.r.squared, digits = 2)))
  mylabel_2 = bquote(italic(y) == .(format(lmod$coefficients[2], digits = 2))~italic(x) + .(format(lmod$coefficients[1], digits = 2)))
  
  # text(x = -1.0, y = 19, labels = mylabel_2)
  # text(x = -2.5, y = 18, labels = mylabel_1)
  
   text(x = 14, y = -2, labels = mylabel_2,cex=1.5)
   text(x = 11.5, y = -3.5, labels = mylabel_1,cex=1.5)
  
  
  # 
  title(main=Title,line=0.5,adj=.05,cex.main=1.6)
  
}

#----- ---------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------

# plot temperature response of canopy flux

plot_tresponse_wtc<-function(Legend=NULL){
  
  # windows(40,40);par(mfrow=c(1,1),mar=c(0,0,0,0),oma=c(5,7,2,2));par(las=1)
  smoothplot(TCAN,hrPs_la,pointcols=c(alpha("black",0.05)),
             linecol=c("black"),polycolor=c(alpha("gray",0.9))
             ,cex=1,main="",random="chamber",
             xlim=c(15,42),ylim=c(-5,25),xlab="",ylab="",
             data=dat.plot.a, kgam=4,axes=F)
  
  
  # box();axis(side=1,labels=T);axis(side=2,labels=T)
  magaxis(side=c(1:4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.5,majorn=4)
  
  
  title(xlab=expression(T[Canopy]~(degree*C)),outer=F,cex.lab=2)
  title(ylab=expression(A~(mu*mol~m^2~s^-1)),outer=F,cex.lab=2,line=1.7)
  
  if(is.null(Legend)==T){
    legend("topright",cex=1.5,pch=16,legend=c("measured","modelled"),col=c(alpha("red",0.5),alpha("black",0.5)),bty="n")
    
  }
  
  # else{legend("topright",legend=NULL,bty="n")}
  # title(main="AMBIENT_CHAMBERS",outer=T,cex.lab=2)
  
  par(new=T)
  
  #plot measured photosynthesis
  
  smoothplot(TCAN,hrPs_la_m,pointcols=c(alpha("red",0.05)),
             linecol=c("red"),polycolor=c(alpha("gray",0.5)),cex=1,main="",
             xlim=c(15,42),ylim=c(-5,25),xlab="",ylab="",
             data=dat.plot.a, kgam=4,axes=F,random="chamber")
  
  }

# windows(100,60);par(mar=c(5.5,5.5,2,1),oma=c(0,0,0,0),mfrow=c(1,2));par(las=1)
# plot_flux()
# plot_tresponse_wtc()

#----------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------

# plot residuals of flux

plot_residuals<-function(){
  
  # windows(40,40);par(mfrow=c(1,1),mar=c(0,0,0,0),oma=c(5,7,2,2));par(las=1)
  with(df,plot(residuals~TCAN,xlim=c(0,45),ylim=c(-10,+10),col=alpha("lightgray",0.1),xlab="",ylab="",axes=F))
  abline(0,0)
  magaxis(side=c(1:4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.5,majorn=5)
  
  lmod<-summary(lm(residuals~TCAN,data=df))
  
  abline(lmod$coefficients[1],lmod$coefficients[2]
         ,col="black",lwd=3,lty=2)
  
  title(xlab=expression(T[Canopy]~(degree*C)),outer=F,cex.lab=2)
  title(ylab=expression(Residuals~(Obs-Pred)),outer=F,cex.lab=2,line=1.7)
  
  # legend("topleft",legend=c(paste("Y =",round(lmod$coefficients[2],3),"X",round(lmod$coefficients[1],3)),paste("R2 = ",round(lmod$adj.r.squared,3))
  # ),bty="n",ncol=1)
  
}



#----------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------


# function to fit peak arrhenius 
# Ts is the temperature in C
# Ropt is the optimum Vcmax or Jmax 
# Topt is the temperature optimum for Vcmax or Jmax
# Ea is the activation energy of vcmax or jmax
# returns Vcmax or Jmax at Ts (mumol m-2s-1)

# fit_parr<-function(Ts,Ropt,Topt,Ea){
#    Vcmax<-Ropt*((200*exp(Ea*(Tk(Ts)-Tk(Topt))/(Tk(Ts)*0.008314*Tk(Topt)))/(200-(Ea*(1-exp(200*(Tk(Ts)-Tk(Topt))/(Tk(Ts)*0.008314*Tk(Topt))))))))          
#    return(Vcmax)
#  }

fit_parr<-function(Ts,k25,delS,Ea){
  # Vcmax<-Ropt*((200*exp(Ea*(Tk(Ts)-Tk(Topt))/(Tk(Ts)*0.008314*Tk(Topt)))/(200-(Ea*(1-exp(200*(Tk(Ts)-Tk(Topt))/(Tk(Ts)*0.008314*Tk(Topt))))))))          
  
  Vcmax<-k25 * exp((Ea*(Tk(Ts) - 298.15))/(298.15*0.008314*Tk(Ts))) * 
    (1+exp((298.15*delS - 200)/(298.15*0.008314))) / 
    (1+exp((Tk(Ts)*delS-200)/(Tk(Ts)*0.008314)))
  
  return(Vcmax)
}


#--------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------

# Function to get mean temperature data for wtc3

get_wtc3_met<-function(path,fname,from){
  
  #set WD to the data directory
  o <- getwd()
  on.exit(setwd(o))
  setwd(path)
  
  #read all csv files and bind to one file
  
  wtcmet <- read.csv(paste0(path,"/",fname) )
  # wtcmet<-read.csv("data/wtc3_met_highres.csv")
  
  wtcmet$DateTime <- as.POSIXct(wtcmet$DateTime,format="%Y-%m-%d")
  
  wtcmet<-subset(wtcmet,wtcmet$period=="Day")
  from=as.Date(from)
  
  met_list<-list()
  
  for(i in 1:length(from)){
    
    D<-as.Date(from[i],format="%Y-%m-%d")
    
    end30<-D - as.difftime(30, unit="days")
    end60<-D - as.difftime(15, unit="days")
    end90<-D - as.difftime(7, unit="days")
    end100<-D - as.difftime(0, unit="days")
    
    #get a subset for required period
    dat30<-wtcmet[wtcmet$DateTime >= end30 & wtcmet$DateTime <= D,]
    dat60<-wtcmet[wtcmet$DateTime >= end60 & wtcmet$DateTime <= D,]
    dat90<-wtcmet[wtcmet$DateTime >= end90 & wtcmet$DateTime <= D,]
    dat100<-wtcmet[wtcmet$DateTime >= end100 & wtcmet$DateTime <= D,]
    
    
    #get average across given time period (set only chamber temperature)
    
    t30 <- dplyr::summarize(group_by(dat30,Treat),
                            #Tair_SP=mean(Tair_SP,na.rm=T),
                            #RH_al=mean(RH_al,na.rm=T),
                            #DP_al=mean(DP_al,na.rm=T),
                            Tmin_30=min(Tair_al,na.rm=T),
                            Tmax_30=max(Tair_al,na.rm=T),
                            Tmean_30=mean(Tair_al,na.rm=T))
    
    
    t60 <- dplyr::summarize(group_by(dat60,Treat),
                            #Tair_SP=mean(Tair_SP,na.rm=T),
                            #RH_al=mean(RH_al,na.rm=T),
                            #DP_al=mean(DP_al,na.rm=T),
                            Tmin_15=min(Tair_al,na.rm=T),
                            Tmax_15=max(Tair_al,na.rm=T),
                            Tmean_15=mean(Tair_al,na.rm=T))
    
    t90 <- dplyr::summarize(group_by(dat90,Treat),
                            #Tair_SP=mean(Tair_SP,na.rm=T),
                            #RH_al=mean(RH_al,na.rm=T),
                            #DP_al=mean(DP_al,na.rm=T),
                            Tmin_7=min(Tair_al,na.rm=T),
                            Tmax_7=max(Tair_al,na.rm=T),
                            Tmean_7=mean(Tair_al,na.rm=T))
    
    
    t100 <- dplyr::summarize(group_by(dat100,Treat),
                             #Tair_SP=mean(Tair_SP,na.rm=T),
                             #RH_al=mean(RH_al,na.rm=T),
                             #DP_al=mean(DP_al,na.rm=T),
                             Tmin_1=min(Tair_al,na.rm=T),
                             Tmax_1=max(Tair_al,na.rm=T),
                             Tmean_1=mean(Tair_al,na.rm=T))
    
    taverage <- data.frame(t30,t60,t90,t100)
    taverage[c(5,9)]<-NULL
    taverage$date<-as.Date(D)
    met_list[[i]]<-taverage
  }
  
  ret<-data.frame(do.call(rbind,met_list))
  names(ret)[1]<-"T_treatment"
  # ret$T_treatment<-ifelse(ret$T_treatment=="Ambient","ambient","elevated")
  
  return(ret)
}


#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# function to fit the Quadratic model for the temperature response of canopy photosynthesis.
# model, Anet=Aopt-b(Tleaf-Topt)^2
#- accepts a dataframe, returns a list with [1] named vector of parameter estiamtes and their se's,
#-   and [2] a dataframe with the predictions and 95% confidence intervals.

FitQuad<-function(data,yvar,xvar,tleafseq=seq(10,40,0.5)){
  
  data$yvar <- data[[yvar]]
  data$xvar <- data[[xvar]]
  
  
  try(An<-nls(yvar~Aopt-(b*(xvar-Topt)^2),data=data,start=list(Aopt=20,Topt=25,b=0.05)))
  A.1<-summary(An)
  results<-(A.1$coefficients[1:6])
  names(results)[1:6] <- c("aopt","topt","b","aopt.se","topt.se","b.se")
  
  TT.i <-tleafseq
  predicts <- predictNLS(An, newdata=data.frame(xvar = TT.i),interval="confidence",level=0.95)
  predicts.df <- data.frame(predicts$summary)
  predicts.df$Tleaf <- TT.i
  
  
  
  
  return(list(results,predicts.df))
  
}

#------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------


# get_wtc3_met<-function(path,fname,from){
#   
#   #set WD to the data directory
#   o <- getwd()
#   on.exit(setwd(o))
#   setwd(path)
#   
#   #read all csv files and bind to one file
#   
#   wtcmet <- read.csv(paste0(path,"/",fname) )
#   #wtcmet<-read.csv("WTC_TEMP_CM_WTCMET_L1_v2.csv")
#   
#   wtcmet$DateTime <- as.POSIXct(wtcmet$DateTime,format="%Y-%m-%d")
#   
#   from=as.Date(from)
#   
#   met_list<-list()
#   
#   for(i in 1:length(from)){
#     
#     D<-from[i]
#     
#     end30<-D - as.difftime(30, unit="days")
#     end60<-D - as.difftime(15, unit="days")
#     end90<-D - as.difftime(7, unit="days")
#     end100<-D - as.difftime(0, unit="days")
#     
#     #get a subset for required period
#     dat30<-wtcmet[wtcmet$DateTime >= end30 & wtcmet$DateTime <= D,]
#     dat60<-wtcmet[wtcmet$DateTime >= end60 & wtcmet$DateTime <= D,]
#     dat90<-wtcmet[wtcmet$DateTime >= end90 & wtcmet$DateTime <= D,]
#     dat100<-wtcmet[wtcmet$DateTime >= end100 & wtcmet$DateTime <= D,]
#     
#     
#     #get average across given time period (set only chamber temperature)
#     
#     t30 <- dplyr::summarize(group_by(dat30,Treat),
#                             #Tair_SP=mean(Tair_SP,na.rm=T),
#                             #RH_al=mean(RH_al,na.rm=T),
#                             #DP_al=mean(DP_al,na.rm=T),
#                             Tmin=mean(Tmin,na.rm=T),
#                             Tmax=mean(Tmax,na.rm=T),
#                             Tmean=mean(Tmean,na.rm=T))
#     
#     
#     t60 <- dplyr::summarize(group_by(dat60,Treat),
#                             #Tair_SP=mean(Tair_SP,na.rm=T),
#                             #RH_al=mean(RH_al,na.rm=T),
#                             #DP_al=mean(DP_al,na.rm=T),
#                             Tmin_15=mean(Tmin,na.rm=T),
#                             Tmax_15=mean(Tmax,na.rm=T),
#                             Tmean_15=mean(Tmean,na.rm=T))
#     
#     t90 <- dplyr::summarize(group_by(dat90,Treat),
#                             #Tair_SP=mean(Tair_SP,na.rm=T),
#                             #RH_al=mean(RH_al,na.rm=T),
#                             #DP_al=mean(DP_al,na.rm=T),
#                             Tmin_7=mean(Tmin,na.rm=T),
#                             Tmax_7=mean(Tmax,na.rm=T),
#                             Tmean_7=mean(Tmean,na.rm=T))
#     
#     
#     t100 <- dplyr::summarize(group_by(dat100,Treat),
#                              #Tair_SP=mean(Tair_SP,na.rm=T),
#                              #RH_al=mean(RH_al,na.rm=T),
#                              #DP_al=mean(DP_al,na.rm=T),
#                              Tmin_1=mean(Tmin,na.rm=T),
#                              Tmax_1=mean(Tmax,na.rm=T),
#                              Tmean_1=mean(Tmean,na.rm=T))
#     
#     taverage <- data.frame(t30,t60,t90,t100)
#     taverage[c(5,9)]<-NULL
#     taverage$date<-as.Date(D)
#     met_list[[i]]<-taverage
#   }
#   
#   ret<-data.frame(do.call(rbind,met_list))
#   names(ret)[1]<-"T_treatment"
#   ret$T_treatment<-ifelse(ret$T_treatment=="Ambient","ambient","elevated")
#   
#   return(ret)
# }


#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# simulation 2: test for the requirement of canopy layer adjusted temperature response parameters
# can we simulate WTC flux with only using a single set of biochemical parameters?


# all temperature response parameters specified for 6 canopy layer
# radiation interception - 6 layers

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


source("R/maespa_functions_control.R")
source("R/install_maespa.R")
source("R/loadLibraries.R")
source("R/data_processing.R")


# one season six crown layers

df<-NULL
dat.plot.a<-NULL

source("R/new_functions_for_wtc3.R")
maes_3 <-do.call(rbind,lapply(insidechambers_con, function(x)run_chamber(x,jmaxnodates=1,jmaxnolayers=6,
                                                                         jmax=c(206,182.2,171.3,161.1,151.5,142.4),
                                                                         jmaxdates=c("14/09/13"),
                                                                         vcmaxnodates=1,vcmaxnolayers=6,
                                                                         vcmax=c(115.3,104.7,99.8,95.1,90.7,85.9),
                                                                         vcmaxdates=c("14/09/2013"),
                                                                         theta=0.57,eavj=23800,delsj=635,ajq=0.26,eavc=59700,delsc=634,
                                                                         rdnodates=1,rddates=c("14/09/13"),
                                                                         rday=c(1.4),dayresp=0.7,q10ndates=1,q10=c(0.07),
                                                                         q10dates=c("14/09/13"))))

# # merge measured flux with modelled flux
# # convert units 
# # substract wood respiration
# 
df<-merge_flux(maes_3)
# 
# # get subset of high PAR (PAR>1200 for temperature response)
# 
dat.plot.a<-subset(df,df$PAR.x>1200) # get subset of high PAR and remove drought treatments
# #--------------------------------------------------------------------------------------------------------------------------
# #--------------------------------------------------------------------------------------------------------------------------

pdf("output/manuscript_figures/figure_3.pdf",height=10,width=10)
par(mar=c(4,4.5,0.5,0.5),oma=c(0,0,0,0),mfrow=c(2,2))


plot_flux(Title=NULL)
legend("topleft",expression((bold(a))),bty="n",cex=1.5)
plot_tresponse_wtc(Legend=F)
legend("topleft",expression((bold(b))),bty="n",cex=1.5)
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------


# run MAESPA with two season parameter setting
# all temperature response parameters specified for one canopy layer
# all Vcmax and Jmax specified for two dates (seasons)
# but radiation interception - 6 layers

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
source("R/maespa_functions_control.R")
source("R/install_maespa.R")
source("R/loadLibraries.R")
source("R/data_processing.R")


# two seasons; photosynthetic parameters assigned for six canopy layers
# Rday source=Aspinwall et al

df<-NULL
dat.plot.a<-NULL


source("R/new_functions_for_wtc3.R")

maes_7<-do.call(rbind,lapply(insidechambers_con, function(x)run_chamber(x,jmaxnodates=2,jmaxnolayers=6,
                                                                        jmax=c(206,182.2,171.3,161.1,151.5,142.4,(c(206,182.2,171.3,161.1,151.5,142.4)*0.7)),
                                                                        jmaxdates=c("14/09/13","01/12/13"),
                                                                        vcmaxnodates=2,vcmaxnolayers=6,
                                                                        vcmax=c(115.3,104.7,99.8,95.1,90.7,85.9,c(115.3,104.7,99.8,95.1,90.7,85.9)*.8),
                                                                        vcmaxdates=c("14/09/2013","01/12/13"),
                                                                        theta=0.57,eavj=23800,delsj=635,ajq=0.26,eavc=59700,delsc=634,
                                                                        rdnodates=1,rddates=c("14/09/13","30/11/13"),
                                                                        rday=c(1.41),dayresp=0.7,q10ndates=1,q10=c(0.07),
                                                                        q10dates=c("14/09/13"))))
# # merge measured flux with modelled flux# merge measured flux with modelled flux
# convert units 
# substract wood respiration

df<-merge_flux(maes_7)

# get subset of high PAR (PAR>1200 for temperature response)

dat.plot.a<-subset(df,df$PAR.x>1200)
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# plot measured vs modelled fluc and temperature response at high PAR 

plot_flux(Title=NULL)
legend("topleft",expression((bold(c))),bty="n",cex=1.5)
plot_tresponse_wtc()
legend("topleft",expression((bold(d))),bty="n",cex=1.5)
legend("topright",cex=1.5,pch=16,legend=c("measured","modelled"),col=c(alpha("red",0.5),alpha("black",0.5)),bty="n")

dev.off()

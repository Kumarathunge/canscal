startover <- function(){
  file.copy(dir("original_inputfiles", full.names=TRUE),"maespa",overwrite=TRUE)
}
cleanup <- function()unlink(Sys.glob("*.dat"))



make_met <- function(ch){
  met <- subset(wtcflux, chamber == ch, select=c(DateTime, Tair_al, VPDair, PAR))
  met <- met[order(met$DateTime),-1]  # order by DateTime; remove DateTime
  
  # Fill NA with last non-NA
  met <- within(met, {
    Tair_al <- na.locf(Tair_al)
    VPDair <- na.locf(VPDair)
    PAR <- na.locf(PAR)
  })
  
  # CO2 must be added; data in wtcflux too noisy
  met$CA <- 400
  
  # Units VPD are Pa
  met$VPDair <- 1000 * met$VPDair
  
  replacemetdata(met, "maespa/met.dat", columns=c("TAIR","VPD","PAR","CA"), 
                 "maespa/met.dat", setdates=FALSE, khrs=24)
  replacePAR("maespa/met.dat", "khrsperday", "metformat", 24)
  replacePAR("maespa/met.dat", "startdate", "metformat", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/met.dat", "enddate", "metformat", format(endDate, "%d/%m/%y"))
}


make_trees <- function(ch){
  
  hd <- subset(treeh, chamber == ch)
  replacePAR("maespa/trees.dat","values","allhtcrown",round(hd$Plant_height / 100,2))
  replacePAR("maespa/trees.dat","nodates","allhtcrown",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allhtcrown",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allradx",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allradx",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allradx",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allrady",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allrady",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allrady",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "allhttrunk", 0.01)
  
  replacePAR("maespa/trees.dat", "values", "alldiam", round(hd$diameter/1000,2)) #convert mm to m
  replacePAR("maespa/trees.dat","nodates","alldiam",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alldiam",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "alllarea", round(hd$leafArea,2))
  replacePAR("maespa/trees.dat","nodates","alllarea",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alllarea",format(hd$Date,"%d/%m/%y"))
  
  #replacePAR("maespa/trees.dat", "xycoords", "xy", make_stand())
}


make_con <- function(){
  
  replacePAR("maespa/confile.dat", "startdate", "dates", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat", "enddate", "dates", format(endDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat","iohrly","control",2)
  
  # Use Medlyn 2011 model of gs.
  replacePAR("maespa/confile.dat", "modelgs", "model", 4)
  replacePAR("maespa/confile.dat", "modelrw ", "model", 0)
}


make_phy <- function(){
  
  replacePAR("maespa/phy.dat","g1","bbmgs",2.2) #g1 fot top canopy leaves (no difference between control and warmed treatments)
  
  # only Vcmax25, Jmax25 and delsc showed difference between seasons
  
  replacePAR("maespa/phy.dat","nodates","jmaxcon",3)
  replacePAR("maespa/phy.dat","nolayers","jmaxcon",2)                     #two canopy layers
  # replacePAR("maespa/phy.dat","values","jmax",181.1)
   replacePAR("maespa/phy.dat","values","jmax",c(181.1,85.6,108.7,85.6,133.6,85.6))
   # replacePAR("maespa/phy.dat","values","jmax",c(181.1,85.6,108.7,51.36,133.6,51.36))
   replacePAR("maespa/phy.dat","dates","jmax",c("14/09/13","30/11/13","01/03/14"))
  # 
  
  replacePAR("maespa/phy.dat","nodates","vcmaxcon",3)
  replacePAR("maespa/phy.dat","nolayers","vcmaxcon",2) #two canopy layers
  # replacePAR("maespa/phy.dat","values","vcmax",105.5)
  # replacePAR("maespa/phy.dat","nolayers","vcmaxcon",2) #two canopy layers
  replacePAR("maespa/phy.dat","values","vcmax",c(105.5,57.2,67.0,57.2,80.2,57.2))
  # replacePAR("maespa/phy.dat","values","vcmax",c(105.5,57.2,67.0,36.32,80.2,36.32))
  replacePAR("maespa/phy.dat","dates","vcmax",c("14/09/2013","30/12/13","01/03/14"))
  # 
  # to do : T-response, Rd, g0
  
  replacePAR("maespa/phy.dat","theta","jmaxpars",0.4) ##all parameters are for spring 2013
  replacePAR("maespa/phy.dat","eavj","jmaxpars",27376)
  
  replacePAR("maespa/phy.dat","delsj","jmaxpars",636)
  replacePAR("maespa/phy.dat","ajq","jmaxpars",0.28)
  
  replacePAR("maespa/phy.dat","eavc","vcmaxpars",54655)
  replacePAR("maespa/phy.dat","delsc","vcmaxpars",636)
  #replacePAR("maespa/phy.dat","delsc","vcmaxpars",c(635,629,636))
  #replacePAR("maespa/phy.dat","dates","vcmaxpars",c("14/09/2013","30/12/13","01/03/14"))
  
  #Rdark
  replacePAR("maespa/phy.dat","values","rd",1.09) #at 25C (Aspinwall et al 2016)
  replacePAR("maespa/phy.dat","dayresp","rdpars",0.7)
  
  
}


make_str <- function(){
  
  replacePAR("maespa/str.dat", "elp", "lia", 1.0)  # spherical LIA
  replacePAR("maespa/str.dat", "cshape", "canopy", "CYL")  # canopy shape
  
}



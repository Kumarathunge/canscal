startover <- function(){
  file.copy(dir("original_inputfiles", full.names=TRUE),"maespa",overwrite=TRUE)
}
cleanup <- function()unlink(Sys.glob("*.dat"))



make_met <- function(ch){
  met <- subset(wtcflux, chamber == ch, select=c(DateTime, Tair_al, VPDair, PAR))
  met <- met[order(met$DateTime),-1]  # order by DateTime; remove DateTime
  
  # Fill NA with last non-NA
  met <- within(met, {
    Tair_al <- na.locf(Tair_al)
    VPDair <- na.locf(VPDair)
    PAR <- na.locf(PAR)
  })
  
  # CO2 must be added; data in wtcflux too noisy
  met$CA <- 400
  
  # Units VPD are Pa
  met$VPDair <- 1000 * met$VPDair
  
  replacemetdata(met, "maespa/met.dat", columns=c("TAIR","VPD","PAR","CA"), 
                 "maespa/met.dat", setdates=FALSE, khrs=24)
  replacePAR("maespa/met.dat", "khrsperday", "metformat", 24)
  replacePAR("maespa/met.dat", "startdate", "metformat", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/met.dat", "enddate", "metformat", format(endDate, "%d/%m/%y"))
}



make_trees <- function(ch){
  
  hd <- subset(treeh, chamber == ch)
  replacePAR("maespa/trees.dat","values","allhtcrown",round(hd$Plant_height / 100,2))
  replacePAR("maespa/trees.dat","nodates","allhtcrown",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allhtcrown",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allradx",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allradx",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allradx",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allrady",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allrady",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allrady",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "allhttrunk", 0.01)
  
  replacePAR("maespa/trees.dat", "values", "alldiam", round(hd$diameter/1000,2)) #convert mm to m
  replacePAR("maespa/trees.dat","nodates","alldiam",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alldiam",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "alllarea", round(hd$leafArea,2))
  replacePAR("maespa/trees.dat","nodates","alllarea",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alllarea",format(hd$Date,"%d/%m/%y"))
  
 # replacePAR("maespa/trees.dat", "xycoords", "xy", make_stand())
}



make_con <- function(parlayers,nopints){
  
  replacePAR("maespa/confile.dat", "startdate", "dates", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat", "enddate", "dates", format(endDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat","iohrly","control",1)
  # Use Medlyn 2011 model of gs.
  replacePAR("maespa/confile.dat", "modelgs", "model", 4)
  replacePAR("maespa/confile.dat", "modelrw", "model", 0)
  replacePAR("maespa/confile.dat", "nolay", "diffang", parlayers)
  replacePAR("maespa/confile.dat", "pplay", "diffang", nopints)
}




# make_phy <- function(){
#   
#   replacePAR("maespa/phy.dat","g1","bbmgs",2.2) #g1 fot top canopy leaves (no difference between control and warmed treatments)
#   replacePAR("maespa/phy.dat","dates","bbmgs","14/09/13")
#   
#   # only Vcmax25, Jmax25 and delsc showed significant differences between seasons
#   
#   replacePAR("maespa/phy.dat","nodates","jmaxcon",3)
#   replacePAR("maespa/phy.dat","nolayers","jmaxcon",2)#two canopy layers
#   
#   
#   # replacePAR("maespa/phy.dat","values","jmax",206)
#     # replacePAR("maespa/phy.dat","values","jmax",c(206,116.7,138.6,87.84467,138.6,87.84467)) #source for sub-canopy estimates:Court et al (unpub). 
#   replacePAR("maespa/phy.dat","values","jmax",c(206,116.7,155.9,116.7,155.9,116.7)) #source for sub-canopy estimates:Court et al (unpub). 
#    
#    replacePAR("maespa/phy.dat","dates","jmax",c("14/09/13","30/11/13","01/03/14"))                         #Not sure this is the correct way params should specify. 
#   #                                                                                                         #sub canopy values are estimated for summer (FEB/2014)
#   
#   replacePAR("maespa/phy.dat","nodates","vcmaxcon",3)
#   replacePAR("maespa/phy.dat","nolayers","vcmaxcon",2) #two canopy layers
#   
#   #replacePAR("maespa/phy.dat","values","vcmax",115.3)
#   
#   replacePAR("maespa/phy.dat","values","vcmax",c(115.3,85.94,90.78689,85.94,90.78689,85.94))
#   replacePAR("maespa/phy.dat","dates","vcmax",c("14/09/2013","30/12/13","01/03/14"))
# 
#   # to do : T-response, Rd, g0
#   
#   replacePAR("maespa/phy.dat","theta","jmaxpars",0.45) #all parameters are for summer 2013
#   replacePAR("maespa/phy.dat","eavj","jmaxpars",25510)
#  
#   replacePAR("maespa/phy.dat","delsj","jmaxpars",645)
#   replacePAR("maespa/phy.dat","ajq","jmaxpars",0.28)
#   
#   replacePAR("maespa/phy.dat","eavc","vcmaxpars",56770)
#   replacePAR("maespa/phy.dat","delsc","vcmaxpars",630)
#   #replacePAR("maespa/phy.dat","delsc","vcmaxpars",c(635,629,636))
#   #replacePAR("maespa/phy.dat","dates","vcmaxpars",c("14/09/2013","30/12/13","01/03/14"))
# 
#   #Rdark
#   replacePAR("maespa/phy.dat","values","rd",2.21) ##at 25C (Aspinwall et al 2016):for ambient grown trees
#   replacePAR("maespa/phy.dat","dayresp","rdpars",1)
#   
#   }


make_str <- function(){
  
  replacePAR("maespa/str.dat", "elp", "lia", 1.0)  # spherical LIA
  replacePAR("maespa/str.dat", "cshape", "canopy", "CYL")  # canopy shape
  
}


# run_chamber <- function(ch){
#   o <- getwd()
#   on.exit(setwd(o))
#   
#   startover()
#   
#   make_str()
#   make_con()
#   make_phy()
#   
#   make_trees(ch)
#   make_met(ch)
#   
#   setwd("maespa")
#   shell("MAESPA64.exe", intern=TRUE)
#   #h <- readlayflux()
#   h <- readhrflux()
#   message(sprintf("Chamber %s simulation complete.",ch))
#   
#   h$chamber <- ch
#   h$DateTime <- seq(from=as.POSIXct(as.character(startDate), tz="UTC"), length=nrow(h), by="1 hour")
#   return(h)
# }


#function to read MAESPA layer flux output

readlayflux<-function(filename = "layflx.dat"){
  
  layflux <- read.table(filename, skip = 7, na.strings = "NaN",header = FALSE,fill = T ,as.is=T)
  
  layflux.1<- suppressWarnings(subset(layflux,!is.na(as.numeric(layflux$V1))))
  layflux.1$A<-rep(c("Leaf aera","Jmax","Vcmax","PAR","hrPs","hrLE"),times=(endDate-startDate)+1)
  
  layflux.1<-layflux.1[-c(7:9)]
  names(layflux.1)[1:6]<-c("L1","L2","L3","L4","L5","L6")
  
  
  layflux.1$Date<-as.Date(rep(seq(startDate,endDate,1),each=144))
  
  layflux.1$Hour<-rep(rep(seq(1,24,1),each=6),times=((endDate-startDate)+1))
  
  
  
  layflux.2<-subset(layflux.1,layflux.1$A=="PAR")
  names(layflux.2)[1:6]<-c("PAR_L1","PAR_L2","PAR_L3","PAR_L4","PAR_L5","PAR_L6")
  layflux.2<-layflux.2[-7]
  
  layflux.3<-subset(layflux.1,layflux.1$A=="hrPs")
  names(layflux.3)[1:6]<-c("hrPs_L1","hrPs_L2","hrPs_L3","hrPs_L4","hrPs_L5","hrPs_L6")
  layflux.3<-layflux.3[-7]
  
  layflux.4<-subset(layflux.1,layflux.1$A=="hrLE")
  names(layflux.4)[1:6]<-c("hrLE_L1","hrLE_L2","hrLE_L3","hrLE_L4","hrLE_L5","hrLE_L6")
  layflux.4<-layflux.4[-7]
  
  
  
  layflux.5<-merge(layflux.2,layflux.3,by=c("Date","Hour"))
  layflux.6<-merge(layflux.5,layflux.4,by=c("Date","Hour"))
  
  return(layflux.6)
}















#splat <- read.csv(gzfile("maeswrapdefinitions.gz"), header=T)

#runmaespa(2, runfile="parameters_for_batch_run.csv", runit=FALSE, whichcols=1:2)
#runresults <- maesparunall(runfile="parameters_for_batch_run.csv")

#function to fit GAM and plot GAM
fitgam <- function(X,Y,dfr, k=-1, R=NULL){
  dfr$Y <- dfr[,Y]
  dfr$X <- dfr[,X]
  if(!is.null(R)){
    dfr$R <- dfr[,R]
    model <- 2
  } else model <- 1
  dfr <- droplevels(dfr)
  
  
  if(model ==1){
    g <- gam(Y ~ s(X, k=k), data=dfr)
  }
  if(model ==2){
    g <- gamm(Y ~ s(X, k=k), random = list(R=~1), data=dfr)
  }
  
  return(g)
}


#' Plot a generalized additive model
#' @param x Variable for X axis (unquoted)
#' @param y Variable for Y axis (unquoted)
#' @param data Dataframe containing x and y
#' @param kgam the \code{k} parameter for smooth terms in gam.
#' @param random An optional random effect (quoted)
#' @param log Whether to add log axes for x or y (but no transformations are done).
#' @param fitoneline Whether to fit only 
smoothplot <- function(x,y,g=NULL,data,
                       fittype=c("gam","lm"),
                       kgam=4,
                       random=NULL,
                       randommethod=c("lmer","aggregate"),
                       log="",
                       fitoneline=FALSE,
                       pointcols=NULL,
                       linecols=NULL, 
                       xlab=NULL, ylab=NULL,
                       polycolor=alpha("lightgrey",0.7),
                       axes=TRUE,
                       ...){
  
  fittype <- match.arg(fittype)
  randommethod <- match.arg(randommethod)
  
  if(!is.null(substitute(g))){
    data$G <- as.factor(eval(substitute(g),data))
  } else {
    fitoneline <- TRUE
    data$G <- 1
  }
  data$X <- eval(substitute(x),data)
  data$Y <- eval(substitute(y),data)
  data <- droplevels(data)
  
  data <- data[!is.na(data$X) & !is.na(data$Y) & !is.na(data$G),]
  
  if(is.null(pointcols))pointcols <- palette()
  if(is.null(linecols))linecols <- palette()
  
  if(is.null(xlab))xlab <- substitute(x)
  if(is.null(ylab))ylab <- substitute(y)
  
  # If randommethod = aggregate, average by group and fit simple gam.
  if(!is.null(random) && randommethod == "aggregate"){
    data$R <- data[,random]
    
    data <- summaryBy(. ~ R, FUN=mean, na.rm=TRUE, keep.names=TRUE, data=data,
                      id=~G)
    R <- NULL
  }
  
  
  if(!fitoneline){
    
    d <- split(data, data$G)
    
    if(fittype == "gam"){
      fits <- lapply(d, function(x)try(fitgam("X","Y",x, k=kgam, R=random)))
      if(!is.null(random))fits <- lapply(fits, "[[", "gam")
    } else {
      fits <- lapply(d, function(x)lm(Y ~ X, data=x))
    }
    hran <- lapply(d, function(x)range(x$X, na.rm=TRUE))
  } else {
    if(fittype == "gam"){
      fits <- list(fitgam("X","Y",data, k=kgam, R=random))
      if(!is.null(random))fits <- lapply(fits, "[[", "gam")
    } else {
      fits <- list(lm(Y ~ X, data=data))
    }
    hran <- list(range(data$X, na.rm=TRUE))
    
  }
  
  with(data, plot(X, Y, xaxt="n",yaxt="n", pch=16, col=pointcols[G],
                  xlab=xlab, ylab=ylab, ...))
  
  if(axes){
    if(log=="xy")magaxis(side=1:2, unlog=1:2)
    if(log=="x"){
      magaxis(side=1, unlog=1)
      axis(2)
      box()
    }
    if(log=="y"){
      magaxis(side=2, unlog=2)
      axis(1)
      box()
    }
    if(log==""){
      axis(1)
      axis(2)
      box()
    }
  }
  
  for(i in 1:length(fits)){
    
    if(fittype == "gam"){
      nd <- data.frame(X=seq(hran[[i]][1], hran[[i]][2], length=101))
      if(!inherits(fits[[i]], "try-error")){
        p <- predict(fits[[i]],nd,se.fit=TRUE)
        addpoly(nd$X, p$fit-2*p$se.fit, p$fit+2*p$se.fit, col=polycolor[i])
        lines(nd$X, p$fit, col=linecols[i], lwd=2)
      }
    }
    if(fittype == "lm"){
      pval <- summary(fits[[i]])$coefficients[2,4]
      LTY <- if(pval < 0.05)1 else 5
      predline(fits[[i]], col=linecols[i], lwd=2, lty=LTY)
    }
  }
  
  return(invisible(fits))
}


addpoly <- function(x,y1,y2,col=alpha("lightgrey",0.7),...){
  ii <- order(x)
  y1 <- y1[ii]
  y2 <- y2[ii]
  x <- x[ii]
  polygon(c(x,rev(x)), c(y1, rev(y2)), col=col, border=NA,...)
}

predline <- function(fit, from=NULL, to=NULL, col=alpha("lightgrey",0.7), ...){
  
  if(is.null(from))from <- min(fit$model[,2], na.rm=TRUE)
  if(is.null(to))to <- max(fit$model[,2], na.rm=TRUE)
  
  newdat <- data.frame(X = seq(from,to, length=101))
  names(newdat)[1] <- names(coef(fit))[2]
  
  pred <- as.data.frame(predict(fit, newdat, se.fit=TRUE, interval="confidence")$fit)
  
  addpoly(newdat[[1]], pred$lwr, pred$upr, col=col)
  
  #ablinepiece(fit, from=from, to=to, ...)
  lines(pred$fit~newdat[,1])
}

#'@title Add a line to a plot
#'@description As \code{abline}, but with \code{from} and \code{to} arguments. 
#'If a fitted linear regression model is used as asn argument, it uses the min and max values of the data used to fit the model.
#'@param a Intercept (optional)
#'@param b Slope (optional)
#'@param reg A fitted linear regression model (output of \code{\link{lm}}).
#'@param from Draw from this X value
#'@param to Draw to this x value
#'@param \dots Further parameters passed to \code{\link{segments}}
#'@export
ablinepiece <- function(a=NULL,b=NULL,reg=NULL,from=NULL,to=NULL,...){
  
  # Borrowed from abline
  if (!is.null(reg)) a <- reg
  
  if (!is.null(a) && is.list(a)) {
    temp <- as.vector(coefficients(a))
    from <- min(a$model[,2], na.rm=TRUE)
    to <- max(a$model[,2], na.rm=TRUE)
    
    if (length(temp) == 1) {
      a <- 0
      b <- temp
    }
    else {
      a <- temp[1]
      b <- temp[2]
    }
  }
  
  segments(x0=from,x1=to,
           y0=a+from*b,y1=a+to*b,...)
  
}




source("R/loadLibraries.R")
source("R/functions.R")
source("R/functions_for_analysis.R")
source("R/rsq_mm.R")



# fit ACi curves

path<-getwd()

f3_25 <- makecurves(path,"/data/WTC_TEMP_CM_GX-ACI25_20130705-20140422_L1_v2.csv",figname="WTC3_ACi_25C")
d3_25 <- makedata(path,"/Data/WTC_TEMP_CM_GX-ACI25_20130705-20140422_L1_v2.csv",f3_25)

# get mean values of Vcmax and Jmax (at 25C)
# 
# d3_25<-summaryBy(Vcmax+Jmax~Date+T_treatment,data=d3_25,FUN=c(mean,std.error))

#-------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------

#function to get running averages for any defined time (wtc2, wtc3 and wtc4)

wtcMet<- function(path,fname,from){
  
  #set WD to the data directory
  o <- getwd()
  on.exit(setwd(o))
  setwd(path)
  
  #read all csv files and bind to one file
  
  wtcmet <- read.csv(paste0(path,"/",fname) )
  #wtcmet<-read.csv("WTC_TEMP_CM_WTCMET_L1_v2.csv")
  
  wtcmet$DateTime <- as.POSIXct(wtcmet$DateTime,format="%Y-%m-%d")
  
  from=as.Date(from)
  
  end30<-from - as.difftime(30, unit="days")
  end60<-from - as.difftime(60, unit="days")
  end90<-from - as.difftime(90, unit="days")
  
  #get a subset for required period
  dat30<-wtcmet[wtcmet$DateTime >= end30 & wtcmet$DateTime <= from,]
  dat60<-wtcmet[wtcmet$DateTime >= end60 & wtcmet$DateTime <= from,]
  dat90<-wtcmet[wtcmet$DateTime >= end90 & wtcmet$DateTime <= from,]
  
  
  
  #get average across given time period (set only chamber temperature)
  
  t30 <- dplyr::summarize(group_by(dat30,Treat),
                          #Tair_SP=mean(Tair_SP,na.rm=T),
                          #RH_al=mean(RH_al,na.rm=T),
                          #DP_al=mean(DP_al,na.rm=T),
                          Tmin=mean(Tmin,na.rm=T),
                          Tmax=mean(Tmax,na.rm=T),
                          Tmean=mean(Tmean,na.rm=T))
  
  
  t60 <- dplyr::summarize(group_by(dat60,Treat),
                          #Tair_SP=mean(Tair_SP,na.rm=T),
                          #RH_al=mean(RH_al,na.rm=T),
                          #DP_al=mean(DP_al,na.rm=T),
                          Tmin=mean(Tmin,na.rm=T),
                          Tmax=mean(Tmax,na.rm=T),
                          Tmean=mean(Tmean,na.rm=T))
  
  t90 <- dplyr::summarize(group_by(dat90,Treat),
                          #Tair_SP=mean(Tair_SP,na.rm=T),
                          #RH_al=mean(RH_al,na.rm=T),
                          #DP_al=mean(DP_al,na.rm=T),
                          Tmin=mean(Tmin,na.rm=T),
                          Tmax=mean(Tmax,na.rm=T),
                          Tmean=mean(Tmean,na.rm=T))
  
  
  taverage <- data.frame(t30,t60,t90)
  
  
  
  return(taverage)
}

#wtcMet(path="C:/Users/90931217/Documents/Dushan/Repos/PhotoM/MET_DATA/WTC4",
#fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2016-04-01")

#------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------
#to get monthly averages for WTC1 HFE and OZ flux sites

# add preceding 30 days air temperature


d3_25$Tavg_30<-NA
d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2013-07-05")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-07-05")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2013-07-05")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-07-05")[[4]][[2]]


d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2013-08-21")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-08-21")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2013-08-21")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-08-21")[[4]][[2]]


d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2013-09-19")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-09-19")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2013-09-19")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-09-19")[[4]][[2]]


d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2013-10-18")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-10-18")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2013-10-18")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-10-18")[[4]][[2]]


d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2013-12-03")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-12-03")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2013-12-03")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2013-12-03")[[4]][[2]]

d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2014-01-13")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-01-13")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2014-01-13")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-01-13")[[4]][[2]]

d3_25$Tavg_30[which(d3_25$T_treatment=="ambient" & d3_25$Date=="2014-04-22")]<-wtcMet(path=paste0(path,"/data"),
                                                                                               fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-04-22")[[4]][[1]]
d3_25$Tavg_30[which(d3_25$T_treatment=="elevated" & d3_25$Date=="2014-04-22")]<-wtcMet(path=paste0(path,"/data"),
                                                                                                fname="WTC_TEMP_CM_WTCMET_L1_v2.csv",from="2014-04-22")[[4]][[2]]

d3_25$Date<-as.Date(d3_25$Date)

#----------------------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------------------

# with(d3_25,plot(Tavg_30,Vcmax))
# with(d3_25,plot(Tavg_30,Jmax))

# pdf("output/manuscript_figures/vcmax_jmax_25.pdf",height=8,width=4)


tiff(file="output/manuscript_figures/Figure_3.tiff",width=8,height=4,units="in",res=300)

par(mar=c(4,5,.5,.5),oma=c(0,0,0,0),mfrow=c(1,2))

with(d3_25,plot(Tavg_30,Vcmax,ylim=c(50,300),xlim=c(10,30),xlab="",ylab="",bg=alpha("grey",0.9),pch=21,cex=1.5,axes=F))

magaxis(side=c(1:4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.2,majorn=3)

title(xlab=expression(T[growth]~(degree*C)),outer=F,cex.lab=1.5)
title(ylab=expression(V[cmax25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=3)


library(lme4)
lmfitsv<-lmer(Vcmax~Tavg_30+(1|T_treatment),data=d3_25)
rsquared.glmm(lmfitsv)

lmfitsj<-lmer(Jmax~Tavg_30+(1|T_treatment),data=d3_25)
rsquared.glmm(lmfitsj)

# lmfits2<-summary(lm(Vcmax~Tavg_30,data=d3_25,weights=1/Vcmax.std.error))


ablineclip(summary(lmfitsv)$coefficients[1],summary(lmfitsv)$coefficients[2],x1=8,x2=27,lwd=2)

mylabel_1 = bquote(italic(r)^2 == .(format(rsquared.glmm(lmfitsv)[[4]], digits = 2)))
mylabel_2 = bquote(italic(y) == .(format(summary(lmfitsv)$coefficients[1], digits = 2)) - .(format(abs(summary(lmfitsv)$coefficients[2]), digits = 2))~italic(x))

# text(x = -1.0, y = 19, labels = mylabel_2)
# text(x = -2.5, y = 18, labels = mylabel_1)

text(x = 18, y = 285, labels = mylabel_2,cex=1)
text(x = 18, y = 270, labels = mylabel_1,cex=1)

legend("topright",expression((bold(a))),bty="n",cex=1.3)
#---------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------

# plot Jmax


with(d3_25,plot(Tavg_30,Jmax,ylim=c(50,300),xlim=c(10,30),xlab="",ylab="",bg=alpha("grey",0.9),pch=21,cex=1.5,axes=F))

magaxis(side=c(1:4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.2,majorn=3)

# title(xlab=expression(T[growth]~(degree*C)),outer=F,cex.lab=1.5)
title(ylab=expression(J[max25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=3)
title(xlab=expression(T[growth]~(degree*C)),outer=F,cex.lab=1.5)

lmfitsj<-lmer(Jmax~Tavg_30+(1|T_treatment),data=d3_25)
rsquared.glmm(lmfitsj)

lmfitsj<-lmer(Jmax~Tavg_30+(1|T_treatment),data=d3_25)
rsquared.glmm(lmfitsj)

# lmfits2<-summary(lm(Vcmax~Tavg_30,data=d3_25,weights=1/Vcmax.std.error))


ablineclip(summary(lmfitsj)$coefficients[1],summary(lmfitsj)$coefficients[2],x1=8,x2=27,y1=230,y2=80,lwd=2)

mylabel_1 = bquote(italic(r)^2 == .(format(rsquared.glmm(lmfitsj)[[4]], digits = 2)))
mylabel_2 = bquote(italic(y) == .(format(summary(lmfitsj)$coefficients[1], digits = 2)) - .(format(abs(summary(lmfitsj)$coefficients[2]), digits = 2))~italic(x))

# text(x = -1.0, y = 19, labels = mylabel_2)
# text(x = -2.5, y = 18, labels = mylabel_1)

text(x = 18, y = 285, labels = mylabel_2,cex=1)
text(x = 18, y = 270, labels = mylabel_1,cex=1)


legend("topright",expression((bold(b))),bty="n",cex=1.3)


dev.off()
#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

#WTC3 sun-shade comparison-default gammastar

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

path<-getwd()
fs <- makecurves(path,"/data/shadeleafaci.csv",figname="WTC3")
ds <- makedata(path,"/Data/shadeleafaci.csv",fs)
ds$layer<-factor("shade")

fsh <- makecurves(path,"/data/sunleafaci.csv",figname="WTC3")
dsh <- makedata(path,"/Data/sunleafaci.csv",fsh)
dsh$layer<-factor("sun")

# with(dsh,plot(Vcmax,Jmax,col=temp_treatment,pch=16,ylim=c(50,200),xlim=c(50,200)))
# with(ds,points(Vcmax,Jmax,col=temp_treatment,ylim=c(50,200),xlim=c(50,200)))

sun_shade_data<-rbind.fill(ds,dsh)


# test Vcmax
lmshade<-lm(Vcmax~layer,data=sun_shade_data)
summary(lmshade)
car::Anova(lmshade)


# test Jmax
lmj<-lm(Jmax~layer,data=sun_shade_data)
summary(lmj)
car::Anova(lmj)

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

sun_params<-summaryBy(Vcmax+Jmax~leaf,data=dsh,FUN=c(mean,std.error),na.rm=T)
sun_params$Leaf<-"sun"
shade_params<-summaryBy(Vcmax+Jmax~leaf,data=ds,FUN=c(mean,std.error))
shade_params$Leaf<-"shade"

sun_params<-rbind(sun_params,shade_params)

sun_params$CI_L_vcmax<-with(sun_params,Vcmax.mean-Vcmax.std.error)
sun_params$CI_U_vcmax<-with(sun_params,Vcmax.mean+Vcmax.std.error)

sun_params$CI_L_jmax<-with(sun_params,Jmax.mean-Jmax.std.error)
sun_params$CI_U_jmax<-with(sun_params,Jmax.mean+Jmax.std.error)

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

tiff(file="output/manuscript_figures/sun_shade_vcmax.tiff",width=6,height=8,units="in",res=300)
par(mar=c(2,5,0.5,.5),mfrow=c(1,2),cex.lab=1.5,cex.axis=1.5,oma=c(0,0,0,0))


#-- Vcmax
barplot2(height=sun_params$Vcmax.mean,plot.ci=T,ci.l=sun_params$CI_L_vcmax,ci.u=sun_params$CI_U_vcmax,
         yaxt="n",ci.width=0.2,col=c("red","darkgrey"),
         names.arg=c("Sun","Shade"),ylim=c(0,200))



magaxis(side=c(2,4),labels=c(1,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.2,majorn=3)
# title(main=expression(V[cmax25]),cex.main=1.5)
# title(xlab="Canopy position",outer=F,line=3)
title(ylab=expression(V[cmax25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=2.2)


text(110,"**",cex=2,adj=-.5)
legend("topright",expression((bold(a))),bty="n",cex=1.3)

#Jmax
barplot2(height=sun_params$Jmax.mean,plot.ci=T,ci.l=sun_params$CI_L_jmax,ci.u=sun_params$CI_U_jmax,
         yaxt="n",ci.width=0.2,col=c("red","darkgrey"),
         names.arg=c("Sun","Shade"),ylim=c(0,200))



magaxis(side=c(2,4),labels=c(1,0),frame.plot=T,las=1,cex.axis=1.3,ratio=0.4,tcl=0.2,majorn=3)
# title(main=expression(J[max25]),cex.main=1.5)
title(ylab=expression(J[max25]~(mu*mol~m^2~s^-1)),outer=F,cex.lab=1.5,line=2.2)

text(170,"**",cex=2,adj=-.5)
legend("topright",expression((bold(b))),bty="n",cex=1.3)



dev.off()

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------





dev.off()
startover <- function(){
  file.copy(dir("original_inputfiles", full.names=TRUE),"maespa",overwrite=TRUE)
}
cleanup <- function()unlink(Sys.glob("*.dat"))



make_met <- function(ch){
  met <- subset(wtcflux, chamber == ch, select=c(DateTime, Tair_al, VPDair, PAR))
  met <- met[order(met$DateTime),-1]  # order by DateTime; remove DateTime
  
  # Fill NA with last non-NA
  met <- within(met, {
    Tair_al <- na.locf(Tair_al)
    VPDair <- na.locf(VPDair)
    PAR <- na.locf(PAR)
  })
  
  # CO2 must be added; data in wtcflux too noisy
  met$CA <- 400
  
  # Units VPD are Pa
  met$VPDair <- 1000 * met$VPDair
  
  replacemetdata(met, "maespa/met.dat", columns=c("TAIR","VPD","PAR","CA"), 
                 "maespa/met.dat", setdates=FALSE, khrs=24)
  replacePAR("maespa/met.dat", "khrsperday", "metformat", 24)
  replacePAR("maespa/met.dat", "startdate", "metformat", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/met.dat", "enddate", "metformat", format(endDate, "%d/%m/%y"))
}



make_trees <- function(ch){
  
  hd <- subset(treeh, chamber == ch)
  replacePAR("maespa/trees.dat","values","allhtcrown",round(hd$Plant_height / 100,2))
  replacePAR("maespa/trees.dat","nodates","allhtcrown",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allhtcrown",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allradx",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allradx",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allradx",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat","values","allrady",round(hd$crownwidth / 100 / 2, 2))
  replacePAR("maespa/trees.dat","nodates","allrady",nrow(hd))
  replacePAR("maespa/trees.dat","dates","allrady",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "allhttrunk", 0.01)
  
  replacePAR("maespa/trees.dat", "values", "alldiam", round(hd$diameter/1000,2)) #convert mm to m
  replacePAR("maespa/trees.dat","nodates","alldiam",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alldiam",format(hd$Date,"%d/%m/%y"))
  
  replacePAR("maespa/trees.dat", "values", "alllarea", round(hd$leafArea,2))
  replacePAR("maespa/trees.dat","nodates","alllarea",nrow(hd))
  replacePAR("maespa/trees.dat","dates","alllarea",format(hd$Date,"%d/%m/%y"))
  
   replacePAR("maespa/trees.dat", "xycoords", "xy", make_stand())
}

make_con <- function(){
  
  replacePAR("maespa/confile.dat", "startdate", "dates", format(startDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat", "enddate", "dates", format(endDate, "%d/%m/%y"))
  replacePAR("maespa/confile.dat","iohrly","control",2)
  # Use Medlyn 2011 model of gs.
  replacePAR("maespa/confile.dat", "modelgs", "model", 4)
  replacePAR("maespa/confile.dat", "modelrw", "model", 0)
}


make_phy <- function(){
  
  replacePAR("maespa/phy.dat","g1","bbmgs",2.2) #g1 fot top canopy leaves (no difference between control and warmed treatments)
  replacePAR("maespa/phy.dat","dates","bbmgs","14/09/13")
  
  # only Vcmax25, Jmax25 and delsc showed significant differences between seasons
  
  replacePAR("maespa/phy.dat","nodates","jmaxcon",1)
  replacePAR("maespa/phy.dat","nolayers","jmaxcon",2)#two canopy layers
  
  
  #Jmax for two canopy layers
  replacePAR("maespa/phy.dat","values","jmax",c(206,116.71))                    #source for sub-canopy estimates:Court et al (unpub). 
  replacePAR("maespa/phy.dat","dates","jmax","14/09/13")                         #Not sure this is the correct way params should specify. 
                                                                                  #sub canopy values are estimated for summer (FEB/2014)
  
  #Vcmax for two canopy layers
  
  replacePAR("maespa/phy.dat","nodates","vcmaxcon",1)
  replacePAR("maespa/phy.dat","nolayers","vcmaxcon",2) #two canopy layers
  
  
  replacePAR("maespa/phy.dat","values","vcmax",c(115.3,85.94))
  replacePAR("maespa/phy.dat","dates","vcmax","14/09/2013")
  
  # to do : T-response, Rd, g0
  
  replacePAR("maespa/phy.dat","theta","jmaxpars",0.5) #all parameters are for spring 2013
  replacePAR("maespa/phy.dat","eavj","jmaxpars",21816)
  
  replacePAR("maespa/phy.dat","delsj","jmaxpars",628)
  replacePAR("maespa/phy.dat","ajq","jmaxpars",0.28)
  
  replacePAR("maespa/phy.dat","eavc","vcmaxpars",57718)
  replacePAR("maespa/phy.dat","delsc","vcmaxpars",635)
   
  #Rdark
  replacePAR("maespa/phy.dat","values","rd",1.41) ##at 25C (Aspinwall et al 2016):for ambient grown trees
  replacePAR("maespa/phy.dat","dayresp","rdpars",0.7)
}


make_str <- function(){
  
  replacePAR("maespa/str.dat", "elp", "lia", 1.0)  # spherical LIA
  replacePAR("maespa/str.dat", "cshape", "canopy", "CYL")  # canopy shape
  
}




#run MAESPA for ambient T chambers



source("R/maespa_functions_control.R")
source("R/install_maespa.R")
source("R/loadLibraries.R")
source("R/data_processing.R")

setToken("xLh3r7p426yVepvT7qVs")

if(!dir.exists("cache"))dir.create("cache")
setToPath("cache")

if(!dir.exists("maespa"))dir.create("maespa")


# Download MAESPA if not available (or use this to update)
if(!file.exists("maespa/MAESPA64.exe")){
  install_maespa("maespa")
}


#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# simulation 1: test for the requirement of seasonally adjusted temperature response parameters
# can we simulate WTC flux with only using a single set of biochemical parameters?


# run MAESPA with summer parameter settings 
# all temperature response parameters specified for one canopy layer
# but radiation interception - 6 layers

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

source("R/new_functions_for_wtc3.R")
maes_1 <-do.call(rbind,lapply(insidechambers_con, function(x)run_chamber(x,jmaxnodates=1,jmaxnolayers=1,
                                                                         jmax=c(155.9),
                                                                         jmaxdates=c("14/09/13"),
                                                                         vcmaxnodates=1,vcmaxnolayers=1,
                                                                         vcmax=c(90.78),
                                                                         vcmaxdates=c("14/09/2013"),rdnodates=1,rddates=c("14/09/13"),
                                                                         theta=0.475,eavj=19957,delsj=635,ajq=0.3,eavc=56010,delsc=635,
                                                                         rday=c(1.8),dayresp=1,q10ndates=.7,
                                                                         q10=c(0.033),q10dates=c("14/09/13"))))

# merge measured flux with modelled flux
# convert units 
# substract wood respiration

df<-merge_flux(maes_1)

# get subset of high PAR (PAR>1200 for temperature response)

dat.plot.a<-subset(df,df$PAR.x>1200 & df$Date<as.Date("2014-01-14") )

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# plot measured vs modelled fluc and temperature response at high PAR 

# windows(100,60);par(mar=c(5.5,5.5,2,1),oma=c(0,0,0,0),mfrow=c(1,2));par(las=1)
pdf("output/new_figures/sim_1_one_season.pdf",height=11,width=10)
# par(mar=c(5.5,5.5,2,1),oma=c(0,0,0,0),mfrow=c(3,3))

par(mar=c(6,4.5,2,0.5),oma=c(0,0,0,0),mfrow=c(3,3))
plot_flux(Title="One crown layer")
legend("topleft",expression((bold(a))),bty="n",cex=1.5)

plot_residuals()
legend("topleft",expression((bold(b))),bty="n",cex=1.5)

plot_tresponse_wtc()
legend("topleft",expression((bold(c))),bty="n",cex=1.5)
# dev.off()

# rm(list = ls())


#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# simulation 2: test for the requirement of canopy layer adjusted temperature response parameters
# can we simulate WTC flux with only using a single set of biochemical parameters?


# run MAESPA with summer parameter settings 
# all temperature response parameters specified for two canopy layer
# but radiation interception - 6 layers

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------
df<-NULL
dat.plot.a<-NULL

source("R/new_functions_for_wtc3.R")
maes_2 <-do.call(rbind,lapply(insidechambers_con, function(x)run_chamber(x,jmaxnodates=1,jmaxnolayers=2,
                                                                         jmax=c(155.9,116.7),
                                                                         jmaxdates=c("14/09/13"),
                                                                         vcmaxnodates=1,vcmaxnolayers=2,
                                                                         vcmax=c(90.78,85.94),
                                                                         vcmaxdates=c("14/09/2013"),
                                                                         theta=0.475,eavj=19957,delsj=635,ajq=0.3,eavc=56010,delsc=635,
                                                                         rday=c(1.8),dayresp=1,q10ndates=1,
                                                                         q10=c(0.033),q10dates=c("14/09/13"),rdnodates=1,rddates=c("14/09/13"))))

# merge measured flux with modelled flux
# convert units 
# substract wood respiration

df<-merge_flux(maes_2)

# get subset of high PAR (PAR>1200 for temperature response)

dat.plot.a<-subset(df,df$PAR.x>1200 & df$Date<as.Date("2014-01-14") )
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# plot measured vs modelled fluc and temperature response at high PAR 

# pdf("output/sim_2_one_season_two_canopy.pdf",height=6,width=12)
# par(mar=c(5.5,5.5,2,1),oma=c(0,0,0,0),mfrow=c(1,2))
plot_flux(Title="Two crown layers")
legend("topleft",expression((bold(d))),bty="n",cex=1.5)

plot_residuals()
legend("topleft",expression((bold(e))),bty="n",cex=1.5)

plot_tresponse_wtc(Legend=F)
legend("topleft",expression((bold(f))),bty="n",cex=1.5)
# dev.off()

# rm(list = ls())
#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

# one season six canopy layers

df<-NULL
dat.plot.a<-NULL

source("R/new_functions_for_wtc3.R")
maes_3 <-do.call(rbind,lapply(insidechambers_con, function(x)run_chamber(x,jmaxnodates=1,jmaxnolayers=6,
                                                                         jmax=c(206,180.5,168.9,158.2,148.1,116.7),
                                                                         jmaxdates=c("14/09/13"),
                                                                         vcmaxnodates=1,vcmaxnolayers=6,
                                                                         vcmax=c(115.3,104.7,99.8,95.1,90.7,85.9),
                                                                         vcmaxdates=c("14/09/2013"),
                                                                         theta=0.475,eavj=19957,delsj=635,ajq=0.3,eavc=56010,delsc=635,
                                                                         rdnodates=1,rddates=c("14/09/13"),
                                                                         rday=c(1.8),dayresp=1,q10ndates=1,q10=c(0.033),
                                                                         q10dates=c("14/09/13"))))

# # merge measured flux with modelled flux
# # convert units 
# # substract wood respiration
# 
df<-merge_flux(maes_3)
# 
# # get subset of high PAR (PAR>1200 for temperature response)
# 
dat.plot.a<-subset(df,df$PAR.x>1200 & df$Date<as.Date("2014-01-14")) # get subset of high PAR and remove drought treatments
# #--------------------------------------------------------------------------------------------------------------------------
# #--------------------------------------------------------------------------------------------------------------------------
# 
# # plot measured vs modelled fluc and temperature response at high PAR 
# 
# pdf("output/rday_ACi.pdf",height=4,width=10)
# par(mar=c(5.5,5.5,2,1),oma=c(0,0,0,0),mfrow=c(1,3))
plot_flux(Title="Six crown layers")
legend("topleft",expression((bold(g))),bty="n",cex=1.5)

plot_residuals()
legend("topleft",expression((bold(h))),bty="n",cex=1.5)

plot_tresponse_wtc(Legend=F)
legend("topleft",expression((bold(i))),bty="n",cex=1.5)

dev.off()
#

#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------

#-------------------------------------------------------

#- get data for campaign 1 

#avt.1<-subset(avt.l,avt.l$campaign==1)

avt.l<-read.csv("data/great_aq_data.csv")

avt.l.list <- subset(avt.l, avt.l$campaign==1 & avt.l$LightFac %in% c(1,2,4) & avt.l$W_treatment=="w") 


 with(avt.l.list,plot(Tleaf,Cond,pch=16,cex=2))
Asatdata.l<-split(avt.l.list,paste(avt.l.list$LightFac))

AvTfits.list <- lapply(Asatdata.l,function(x)fitquad(x,"Photo","Tleaf",tleafseq=seq(15,45,1)))

#-------------------------------------------------------



#- pull out the predictions and confidence intervals for plotting
A.pred <- data.frame(do.call(rbind,
                             list(AvTfits.list[[1]][[2]],AvTfits.list[[2]][[2]],AvTfits.list[[3]][[2]])))
A.pred$canopy <- c(rep("low",nrow(A.pred)/3),rep("medium",nrow(A.pred)/3),rep("top",nrow(A.pred)/3))
A.pred$canopy <- factor(A.pred$canopy)  


pdf(file="output/great_photo.pdf",width=4,height=6)
# png(file="output/Drought_Plants/Figure1-Photo_vs_Temperature.png",width=300,height=300)
par(mar=c(0,0,0,1),oma=c(4,4,1,1),mfrow=c(2,1))



#- plot Asat
plotBy(Sim.Mean~Tleaf|canopy,data=A.pred,legend=F,type="l",las=1,xlim=c(15,45),ylim=c(0,30),lwd=3,cex.lab=2,xaxt="n",yaxt="n",
       ylab="",col=COL[c(1,4,6)],
       xlab="")
as.m <- subset(A.pred,canopy=="low")
bs.m <- subset(A.pred,canopy=="medium")
cs.m <- subset(A.pred,canopy=="top")

polygon(x = c(as.m$Tleaf, rev(as.m$Tleaf)), y = c(as.m$Sim.97.5., rev(as.m$Sim.2.5.)), col = alpha(COL[1],0.5), border = NA)
polygon(x = c(bs.m$Tleaf, rev(bs.m$Tleaf)), y = c(bs.m$Sim.97.5., rev(bs.m$Sim.2.5.)), col = alpha(COL[4],0.5), border = NA)
polygon(x = c(cs.m$Tleaf, rev(cs.m$Tleaf)), y = c(cs.m$Sim.97.5., rev(cs.m$Sim.2.5.)), col = alpha(COL[6],0.5), border = NA)


# legend("bottomleft",c("Wet","Dry"),fill=COL,cex=0.9,title="Treatment",bty="n")

#- add TREATMENT MEANS for mass
dat3 <- summaryBy(Photo+Cond+Tair+Tleaf~Room+LightFac,FUN=c(mean,std.error),data=avt.l.list,na.rm=T)

adderrorbars(x=dat3$Tleaf.mean,y=dat3$Photo.mean,SE=dat3$Photo.std.error,direction="updown")
adderrorbars(x=dat3$Tleaf.mean,y=dat3$Photo.mean,SE=dat3$Tleaf.std.error,direction="leftright")

points(Photo.mean~Tleaf.mean,data=dat3,add=T,pch=21,cex=1.2,legend=F,col="black",bg=COL[c(1,4,6)])




#- gussy up the graph
magaxis(side=c(1,2,3,4),labels=c(0,1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2)
# title(xlab=expression(T[leaf]~(degree*C)),cex.lab=1.3,line=2,outer=T,adj=0.9)
title(ylab=expression(A[sat]~(mu*mol~m^-2~s^-1)),cex.lab=1.3,line=2,outer=T,adj=0.9)
legend("topleft",paste("(",letters[1],")",sep=""),bty="n",cex=1.2,text.font=2)
#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

#plot conductance

plot(Cond.mean~Tleaf.mean,data=dat3,pch=21,cex=.2,legend=F,col="black",bg=COL[c(1,4,6)],
     xlim=c(15,45),ylim=c(0,3),xlab="",ylab="",axes=F)
adderrorbars(x=dat3$Tleaf.mean,y=dat3$Cond.mean,SE=dat3$Cond.std.error,direction="updown")
adderrorbars(x=dat3$Tleaf.mean,y=dat3$Cond.mean,SE=dat3$Tleaf.std.error,direction="leftright")

points(Cond.mean~Tleaf.mean,data=dat3,pch=21,add=T,cex=1.2,legend=F,col="black",bg=COL[c(1,4,6)])


magaxis(side=c(1,2,3,4),labels=c(1,1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2)


title(xlab=expression(T[leaf]~(degree*C)),cex.lab=1.3,line=2,outer=T)
title(ylab=expression(g[s]~(mol~m^-2~s^-1)),cex.lab=1.3,line=2,outer=T,adj=0.2)
legend("topleft",paste("(",letters[2],")",sep=""),bty="n",cex=1.2,text.font=2)
legend(25,3,legend=c(100,500,1500),pch=21,bty="n",cex=0.8,pt.bg=COL[c(1,4,6)],
       title=expression(PAR~(mu*mol~m^-2~s^-1)),ncol=3,pt.cex=1.5)

dev.off()

